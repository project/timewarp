<?php
// $Id$

/*
 * timewarp - manage site and content with git and submodules
 * 
 */

require 'includes/drush-hooks.inc';

function timewarp_drush_command() {
  $items['timewarp-init'] = array(
    'aliases' => array('twi'),
    'description' => 'Init the TIMEWARP repository structure.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_ROOT,
  );
  $items['timewarp-snapshot'] = array(
    'aliases' => array('tws'),
    'description' => 'Take a snapshot of your site.',
    'arguments' => array(
      'message' => 'Git commit message.'
    ),
    'options' => array(
      'omit-db' => 'Omit DB dump before doing a snapshot? Defaults to FALSE.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_ROOT,
  );
  $items['timewarp-jump'] = array(
    'aliases' => array('twj'),
    'description' => 'Restore a taken snapshot.',
    'arguments' => array(
      'target' => 'Specification of the commit as git understands it. Beware of relative refspecs like HEAD^ - an autocommit moves HEAD!',
      'omit-db' => 'Omit DB after restoring a snapshot? Defaults to FALSE.',
  ),
    'options' => array(
      'autocommit' => 'Should we autocommit before restoring a snapshot? Defaults to TRUE.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_ROOT,
  );
  $items['timewarp-sqlimport'] = array(
    'description' => 'Helper Command: Imports a sql file.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_CONFIGURATION,
    'arguments' => array(
      'result-file' => 'Should we autocommit before restoring a snapshot? Defaults to TRUE.',
    ),
  );
  $items['timewarp-dbdump'] = array(
    'description' => 'Helper Command: Dump databases for all sites in this drupal installation.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_ROOT,
  );
  $items['timewarp-dbrestore'] = array(
    'description' => 'Helper Command: Restore databases for all sites in this drupal installation.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_ROOT,
  );
  return $items;
}

