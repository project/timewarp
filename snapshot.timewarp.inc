<?php
// $Id$

require_once 'dbdump.timewarp.inc';
require_once 'includes/githelpers.inc';
require_once 'includes/timewarp.inc';

/*
 * Snapshot command
 */

function drush_timewarp_snapshot($msg=NULL) {
  if($msg===$NULL):
    $msg='Timewarp Snapshot';
  endif;
  if(!drush_get_option('omit-db')):
    drush_log('Dumping DB first...');
    if(!drush_timewarp_dbdump()):
      return FALSE;
    endif;
    drush_log('DB dumped.', 'success');
  endif;
  $dirs = new TimewarpDirs();
  // use drush command instead of git commit as githooks won't fire at empty commit
  drush_log('Committing to content repository...');
  if(timewarp_gitcommit_recursive($dirs->content, $msg)):
    drush_log('State committed.', 'success');
    return true;
  else:
    // error should be already set
    return false;
  endif;
}