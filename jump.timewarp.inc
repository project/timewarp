<?php
// $Id$

require_once 'snapshot.timewarp.inc';
require_once 'includes/githelpers.inc';
require_once 'includes/timewarp.inc';
/*
 * Timewarp command
 */
function drush_timewarp_jump($targetcommit) {
  // TODO: user option to not take snapshot
  if(drush_get_option('autocommit', TRUE)) {
    if(!drush_timewarp_snapshot("TIMEWARP autocommit before timewarp to $targetcommit")):
      return FALSE;
    else:
      drush_log('Autocommit done.', 'success');
    endif;
  }
  $dirs = new TimewarpDirs();
  drush_log("Doing timewarp to $targetcommit");
  if(!timewarp_gitcheckout($dirs->content, $targetcommit)):
    return FALSE;
  else:
    // we could rely on githooks here but we chose to do stuff in drush for now
    if(!drush_get_option('omit-db')):
      drush_log('Restoring DB...');
      if(!drush_timewarp_dbrestore()):
        return FALSE;
      endif;
      drush_log('DB restored.', 'success');
    endif;
  endif;
  return TRUE;
}
