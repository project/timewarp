<?php
// TIMEWARP helper start
// we have to pot this into the *global* drushrc.php
// because i did not find early hooks to adjust drupalroot
function timewarp_istimewarpdir($path) {return file_exists("$path/content/timewarp-dbdumps");} 
$path = drush_cwd();
do {
  drush_log("TIMEWARP: looking for timewarp site at $path");
  if(timewarp_istimewarpdir($path)):
    $newpath = "$path/content/code/drupal";
    drush_log("TIMEWARP: found, changing to $newpath");
    $_SERVER['PWD'] = $newpath;
    drush_set_context('DRUSH_OLDCWD', $newpath);
    chdir("$newpath");
    break;
  endif;
} while ($path = _drush_shift_path_up($path));
// TIMEWARP helper end
