<?php
// $Id$

require_once 'includes/toolbox.inc';
require_once 'includes/timewarp.inc';

/*
 * timewarp-dbrestore command
 */
function drush_timewarp_dbrestore() {
  $timewarp = new TimewarpDirs();
  if(!$timewarp->valid)
    return drush_set_error('TIMEWARP_ERROR', 
      dt('TIMEWARP: Cannot dump, we seem not to be in a timewarp site.'));

  $site_records = _drush_find_local_sites_at_root($timewarp->core);
  
  foreach($site_records as $site_record):
    list($site_root, $site_name) = timewarp_parse_siterecord($site_record);
    $result_file = $timewarp->get_dbdump_path($site_name);
    
    $result = drush_invoke_process('@self', 'timewarp-sqlimport', 
      array($result_file), array(
        'root' => $site_root, 
        'uri' => $site_name,
        'verbose' => drush_get_option(array('v', 'verbose'), FALSE),
        'debug' => drush_get_option(array('d', 'debug'), FALSE),
      ), TRUE);
  
    if(!$result) return FALSE;
  endforeach;
  return TRUE;
}