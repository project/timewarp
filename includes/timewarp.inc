<?php
// $Id$

require_once 'toolbox.inc';

class TimewarpDirs {
  /*
   * Constructor
   * 
   * returns a class with relevant dirs for timewarp.
   * usually called without parameter this is inferred from 
   * drupal root, which has to be already bootstrapped.
   * for init this is called with a dir path parameter which
   * should not yet exist and will become timewarp root.  
   * 
   * @param string
   */
  public function __construct($dir = NULL) {
    if(!$dir):
      $dir = $this->_timewarp_locate_timewarproot();
    endif;

    if(!$this->valid = (bool) $dir) return;
    $this->timewarp = $dir;
    $this->content = "$this->timewarp/content";
    $this->sites = "$this->content/sites";
    $this->dbdumps = "$this->content/timewarp-dbdumps"; // magic name used by optional githook
    $this->code = "$this->content/code";
    $this->core = "$this->code/drupal";
    $this->sitescode = "$this->code/sites";
  }
  /*
   * get dbdump path from site name
   * 
   * this does not check that 
   * drupalroot is inside timewarp dir
   */
  public function get_dbdump_path($site_name) {
    return "$this->dbdumps/$site_name.sql";
  }
  /*
   * returns timewarp root or FALSE if no timewarp site 
   */
  private function _timewarp_locate_timewarproot() {
    if(drush_get_context('DRUSH_BOOTSTRAP_PHASE') >= DRUSH_BOOTSTRAP_DRUPAL_ROOT):
      $drupalroot = drush_get_context('DRUSH_DRUPAL_ROOT');
      $drupalroot = timewarp_removetrailingslash(realpath($drupalroot));
      return
        is_link("$drupalroot/sites") && (readlink("$drupalroot/sites") == "../../sites") &&
        is_dir("$drupalroot/../../sites")
        ? realpath("$drupalroot/../../..") : FALSE;
    endif;
  }

}