<?php
// $Id$

require_once 'timewarp.inc';
require_once 'githelpers.inc';

/*
 * Drush Hooks
 * 
 * for this to work we need a drush with fixed
 * http://drupal.org/node/1052826
 * http://drupal.org/cvs?commit=495608
 * 
 * TODO: use and include githelpers
 */

/*
 * HOOK_drush_pm_download_destination_alter
 */
function timewarp_drush_pm_download_destination_alter(&$request, $release) {
  $timewarp = new TimewarpDirs();
  if($timewarp->valid):
    $old_container = $request['project_install_location'];
    $old_container = realpath($old_container);
    $new_container = "$timewarp->sitescode/". timewarp_relative_path($timewarp->sites, $old_container);
    $project = $request['name'];
    $old_destination = "$old_container/$project";
    $new_destination = "$new_container/$project";
    drush_log("TIMEWARP changed install location from $old_destination to $new_destination.");
    $path_adjust = timewarp_relative_path($old_container, $new_destination);
    if(is_link($old_destination) && readlink($old_destination) == $path_adjust):
      drush_log("OK, TIMEWARP symlink already there.");
    elseif(file_exists($old_destination)):
      drush_set_error('TIMEWARP_ERROR', dt('TIMEWARP cannot symlink !path_old to !path_adjust - something is already where the link should go.', array('!path_old' => $path_old, '!path_adjust' => $path_adjust, )));
      return FALSE;
    else:
      drush_log(dt('Creating and symlinking !dir', array('!dir' => $new_container)));
      drush_mkdir($new_container);
      if(!timewarp_drush_op('timewarp_symlink', $path_adjust, $old_destination)) return FALSE;
      if(!drush_shell_cd_and_exec($old_container, "git add $old_destination")) return FALSE;
    endif;
    if($new_container != $old_container) drush_log("TIMEWARP: moved and symlinked destination dir to code area, from $old_destination to $new_destination");
    $request['project_install_location'] = $new_container;
  endif;
  return TRUE;
}
/*
 * HOOK_drush_pm_post_download
 * 
 * (not to confuse with HOOK_post_pm_download !) 
 * * adding githooks
 * * autocommitting
 */
function timewarp_drush_pm_post_download($request, $release) {
  $timewarp = new TimewarpDirs();
  if($timewarp->valid)
    return
      _timewarp_gitinit($request['project_install_location'])  &&
        timewarp_gitcommit($timewarp->content, 'TIMEWARP autocommitting downloaded module.');
}
/*
 * HOOK_post_pm_update
 * 
 * autocommitting
 */
function drush_timewarp_post_pm_update() {
  $timewarp = new TimewarpDirs();
  if($timewarp->valid)
    return timewarp_gitcommit($timewarp->content, 'TIMEWARP autocommitting updated module.');
    
  }
