<?php
// $Id$

/*
 * @file Git helpers
 * 
 * factored out git stuff.
 * maybe port to core drush someday.
 * 
 * this has really grown a lot and 
 * TODO: roll a class / classify.
 */

require_once 'toolbox.inc';

/*
 * Git helpers
 */

/*
 * Check for git version
 * 
 * we're doing a dumb string comparison, 
 * this will break with git 1.10.x < 1.9.x
 * so we then will get a false error and care for that.
 */
function timewarp_gitcheckversion() {
  return drush_shell_exec('git --version')
    && drush_shell_exec_output() >= 'git version 1.7.0';
}

/*
 * Git init - private timewarp version
 * 
 * if it's not a git repo, init one
 * and copy appropriate hooks
 */
function _timewarp_gitinit($repopath, $copyhooks = TRUE) {
  if(!file_exists("$repopath/.git")): // not yet a git repo
    timewarp_assert(drush_shell_cd_and_exec($repopath, "git init"));
    // no assert here, git fails if nothing to commit
    // todo: error handling for other errors
    drush_shell_cd_and_exec($repopath, "git add .; git commit -a -m 'timewarp init'");
  endif;
  if($copyhooks):
    $preparedfiles_dir = _timewarp_preparedfiles_dir();
    drush_copy_dir("$preparedfiles_dir/git/hooks", "$repopath/.git/hooks", True);
    drush_shell_cd_and_exec("$repopath/.git/hooks", 'chmod a+x *');
  endif;
}

/*
 * Git commit
 */
function timewarp_gitcommit($repodir, $msg, $avoid_detached_head = TRUE) {
  drush_log("Commit of $repodir...");
  drush_shell_cd_and_exec($repodir, "git status --porcelain");
  $output = drush_shell_exec_output();
  if(count($output)):
    timewarp_assert(drush_shell_cd_and_exec($repodir, "git add -A"), "$repodir $ git add -A");
    timewarp_assert(drush_shell_cd_and_exec($repodir, "git commit -a -m %s", $msg), "$repodir $ git commit -a");
  endif;
}

/*
 * Git commit recursive
 * 
 * care for submodules before commit
 * 
 * we're doubeling the effort in githooks because they dont fire at empty commits
 * which is why we also have the commitall alias
 * 
 * todo: check if this can be worked around eith --allow-empty
 * and maybe other githooks
 */
function timewarp_gitcommit_recursive($repopath, $msg) {
  drush_log("Recursive commit of $repopath...");
  // get submodules
  $repo = timewarp_gitinfo_recursive($repopath);
  if(!$repo->path): 
    return drush_set_error("Error getting gitinfo for $repopath");
  endif;
  foreach($repo->submodules as $submodule):
    timewarp_gitcommit_recursive($submodule->path, $msg);
  endforeach;
  if($repo->dirty) timewarp_gitcommit($repo->path, $msg);
  return TRUE;
}

/*
 * Git create branch
 */
function timewarp_gitcreatebranch($repodir, $branchname, $checkout = FALSE) {
  if($checkout):
    return drush_shell_cd_and_exec($repodir, "git checkout -b %s", $branchname);
  else:
    return drush_shell_cd_and_exec($repodir, "git branch %s", $branchname);
  endif;
}

/*
 * create a temporary branch name
 */
function timewarp_createtempbranchname($branches, $prefix, $len = 4) {
  $branches = preg_replace("#$prefix([0-9])+#", '$1', $branches);
  $branches[] = 0;
  $next = max($branches)+1;
  return sprintf("$prefix%0${len}s", $next);
}

/*
 * Git submodule update 
 */
function timewarp_gitsubmoduleupdate($repopath) {
  // this may return false if no submodules present, which is ok
  drush_shell_cd_and_exec($repopath, "git submodule init");
  return drush_shell_cd_and_exec($repopath, "git submodule update");
}

/*
 * Git submodule update recursive
 */
function timewarp_gitsubmoduleupdate_recursive($repo) {
  foreach($repo->submodules as $submodule):
    timewarp_gitsubmoduleupdate($repo->path);
    timewarp_gitsubmoduleupdate_recursive($repo);
  endforeach;
}

/*
 * Git checkout 
 */
function timewarp_gitcheckout($repopath, $targetcommit) {
  return drush_shell_cd_and_exec($repopath, "git checkout %s", $targetcommit);
}

/*
 * Git checkout recursive TODO
 * 
 * care for submodules after checkout
 */
function timewarp_gitcheckout_recursive($repopath, $targetcommit) {
  $repo = timewarp_gitinfo_recursive($repopath);
  if(!$repo->path) return FALSE;
  timewarp_gitcheckout($repo->path, $targetcommit);
  timewarp_gitsubmoduleupdate_recursive($repo);
  return TRUE;
}

/*
 * Git info recursive
 * 
 * @param string
 *   path to analyze
 *   
 * @return mixed
 *   * FALSE if not inside a repo, otherwise
 *   * stdClass repo with attributes
 *     * mixed path - string repo path, FALSE if not inside a repo
 *     * bool dirty - if working tree does not match last checkout
 *     * array submodules - array of repo instances for submoeules 
 */
function timewarp_gitinfo_recursive($path) {
  $repo = timewarp_gitinfo($path);
  if(!$repo->path) return $repo;
  
  drush_log("TIMEWARP: getting submodule gitinfo for $path");
  $result = drush_shell_cd_and_exec($path, 'git submodule status');
  $output = drush_shell_exec_output();

  $repo->submodules = array();
  foreach($output as $line):
    // TODO: catch parse errors
    if(preg_match('/.?[0-9a-fA-F]{40} (?P<relpath>[^ ]+) +.*/', $line, $matches)):
      $submodule = $matches['relpath'];
      drush_log("TIMEWARP: found submodule $submodule");
      $repo->submodules[] = timewarp_gitinfo_recursive("$repo->path/". $submodule);
    else:
      throw new Exception("Error parsing git submodule output: $line");
    endif;
  endforeach;
  return $repo;
}

/*
 * Git info
 * 
 * TODO: add branch info
 * 
 * @param string
 *   path to analyze
 *   
 * @return mixed
 *   * FALSE if not inside a repo, otherwise
 *   * stdClass repo with attributes
 *     * mixed path: string repo path, FALSE if not inside a repo
 *     * bool dirty: if working tree does not match last checkout
 */
function timewarp_gitinfo($path) {
  drush_log("TIMEWARP: getting gitinfo for $path");
  $repo = new stdclass;
  $repo->path = timewarp_gitrepopath($path);
  if(!$repo->path) return $repo;
  
  $result = drush_shell_cd_and_exec($path, 'git status --porcelain');
  $output = drush_shell_exec_output();
  $repo->dirty = (bool)count($output);

  $result = drush_shell_cd_and_exec($path, 'git branch');
  $output = drush_shell_exec_output();
  $repo->branches = array();
  $repo->onbranch = NULL;
  foreach($output as $line):
    // TODO: catch parse errors
    preg_match('#(?P<current>.) *(?P<branch>[^ ]+)#', $line, $matches);
    $repo->branches[] = $matches['branch'];
    if($matches['current'] == '*') $repo->onbranch = $matches['branch'];
  endforeach;
  
  return $repo;
}

/*
 * Git repopath
 * 
 * given a path that is in a git repo,
 * spits out the path of that git repo
 * if not spits out false
 */
function timewarp_gitrepopath($path) {
  if(!is_dir($path)) $path = dirname($path);
  $path = timewarp_removetrailingslash($path);

  $result = drush_shell_cd_and_exec($path, 'git rev-parse --show-toplevel');
  if(!$result) return FALSE;

  $output = drush_shell_exec_output();
  $repopath = $output[0];
  return $repopath;
}

/*
 * is absolute path
 * 
 * checks if a path is absolute ie begins with a slash
 */
  function timewarp_isabsolutepath($path) {
    return substr($path, 0, 1) == '/';
  }