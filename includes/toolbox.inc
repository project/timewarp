<?php
// $Id$

/*
 * get prepared files folder
 */
function _timewarp_preparedfiles_dir() {
  $commands = drush_get_commands();
  return $commands['timewarp-init']['path'] .'/preparedfiles';
}

/*
 * it's a project if it has a *.info file 
 */
function _timewarp_is_project($project_dir) {
  return is_dir($project_dir) && glob("$project_dir/*.info");
}

/*
 * the php function symlink sometimes just fails to work for no appearent reason
 * going to the shell instead.
 */
function timewarp_symlink($target, $link) {
  return timewarp_assert(drush_shell_exec('ln -s %s %s', $target, $link));
}

/*
 * helper function for all operations
 * that must abort on fail
 */
function timewarp_assert($result, $msg = 'TIMEWARP: Error.') {
  if(!$result) 
    throw new Exception($msg);
  return $result;
}
/*
 * helper function for all operations
 * that must abort on fail
 */
function timewarp_drush_op($function) {
  $args = func_get_args();
  $msg = $args[0] . '('. join(', ', array_slice($args, 1)) .')';
  return timewarp_assert(call_user_func_array('drush_op', $args), $msg);
}

/*
 * Mova a file/dir and set a symlink to the new location
 */
function timewarp_moveandlink($from, $to, $linkallowed = FALSE) {
  if(!$linkallowed && is_link($from)) return FALSE;
  if(!drush_mkdir(dirname($to))) return FALSE;
  if(!drush_move_dir($from, $to)) return FALSE;
  if(!timewarp_drush_op('timewarp_symlink', timewarp_relative_path(dirname($from), $to), $from)) return FALSE;
  return TRUE;
}

/*
 * Relative Path calculator
 * 
 * returns the relative path from $from to $to - maybe including '..'
 */
function timewarp_relative_path($from, $to) {
  // for now we assume that both are absolute or both are relative
  if((strpos($from, '/') === 0) != (strpos($to, '/') === 0))
    throw new Exception("Cannot find Relpath from $from to $to. Exiting.");
  rtrim($to, '/');
  rtrim($from, '/');
  $from .= '/';
  $shortlength = strlen($from);
  if($from == substr($to, 0, $shortlength)): // $from subpath of $to  
    return substr($to, $shortlength);
  else:
    return '../'. timewarp_relative_path(dirname($from), $to);
  endif;
}

/*
 * Remove trailing slash from path
 */
function timewarp_removetrailingslash($path) {
  return rtrim($path, '/');
}

/*
 * parse a site record
 * 
 * list($site_root, $site_name) = timewarp_parse_siterecord($site_record);
 */
function timewarp_parse_siterecord($site_record) {
  return explode('#', $site_record, 2);
}
