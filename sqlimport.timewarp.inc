<?php
// $Id$
/*
 * timewarp-sqlimport command
 * 
 * the reverse to sql-dump, understands the same --result-file option
 * (sql-cli won't do the job, it only listens to stdin)
 */
function drush_timewarp_sqlimport($result_file) {
    if(!is_file($result_file)):
      drush_log("Not a valid file in --result-file option: $result_file");
      return FALSE;
    endif;
    $import_command = _drush_sql_connect();
    return drush_shell_exec("$import_command < $result_file");
}