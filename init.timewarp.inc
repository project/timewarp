<?php
// $Id$

require_once 'includes/toolbox.inc';
require_once 'includes/githelpers.inc';
require_once 'includes/timewarp.inc';

/*
 * Timewarp-init command
 * 
 * does some magic on a drupal site,
 * builds the timewarp directory, link and repository layout
 */
function drush_timewarp_init() {
  try { // if something fails, give up
    timewarp_assert(timewarp_gitcheckversion(), 'Sorry, we need  at least GIT 1.7+');
    
    $preparedfiles_dir = _timewarp_preparedfiles_dir();
    if(!is_dir($preparedfiles_dir)) throw new Exception('TIMEWARP: Where is my mind? Cannot find own Directory.');
    
    $timewarp = new TimewarpDirs();
    if($timewarp->valid):
      drush_log("TIMEWARP: This seems to be already a Timewarp Site - skipping Dir Layout Creation");
      
    else:
      drush_log("TIMEWARP: This seems NOT to be a Timewarp Site - doing full Dir Layout Creation");

      $drupalroot = DRUPAL_ROOT;
      $targetdir = "$drupalroot.timewarp";
      timewarp_assert(!file_exists($targetdir), "Timewarp target dir already exists: $targetdir");
      drush_log("Creating Target Dir: $targetdir");
      $timewarp = new TimewarpDirs($targetdir);

      drush_mkdir($timewarp->dbdumps);
      
      // move core dir
      timewarp_assert(
        timewarp_moveandlink($drupalroot, $timewarp->core)
        , 'Error creating symlink - already symlinked?');
      
      // move sites dir
      timewarp_assert(
        timewarp_moveandlink("$timewarp->core/sites", $timewarp->sites)
        , 'Error creating symlink - already symlinked?');
    endif;

    // init repository for core
    drush_log('Initializing Drupal Core Repo...');
    _timewarp_gitinit($timewarp->core);
    timewarp_gitcommit_recursive($timewarp->core, 'TIMEWARP init.');
    
    // init module repositories
    drush_log('Initializing Module Repos...');
    $submodules = _timewarp_gitinit_projects_in_all_sites("$timewarp->sites");
    
    // init repository for content
    drush_log('Initializing Content Repo...');
    _timewarp_gitinit($timewarp->content);

    // move submodules and init
    drush_log('Moving submodules...');
    _timewarp_handle_submodules($timewarp, $submodules);

    // set submodule for core
    // TODO: factor out
    drush_log('Initializing Drupal Core Submodule...');
    $project_url = timewarp_projectatdrupalorg('drupal');
    $project_path = "$timewarp->core";
    $project_relative_path = timewarp_relative_path($timewarp->content, $project_path);
    
    drush_shell_cd_and_exec($timewarp->content, "git rm -r --cached  $project_relative_path");
    drush_shell_cd_and_exec($timewarp->content, "git submodule add $project_url $project_relative_path");
    
    // add drushrc settings
    // TODO: append if file exists
    drush_log('Adding Site drushrc.php...');
    timewarp_drush_op('copy', "$preparedfiles_dir/site-specific/drushrc.php", "$timewarp->core/drushrc.php");
    
    // now init submodules and commit
    drush_log('Initializing and committing submodules...');
    timewarp_assert(
      drush_shell_cd_and_exec($timewarp->content, 'git submodule init')
      , 'Could not init submodules.');
    timewarp_gitcommit_recursive($timewarp->content, 'TIMEWARP init.');
    
    // add nice git aliases
    drush_log('Adding some nice git aliases...');
    $gitaliases = array(
      'st' => 'status',
      'ci' => 'commit',
      'co' => 'checkout',
      'br' => 'branch',
      'sm' => 'submodule',
      'sms' => '!git rev-parse --show-toplevel; git status; git submodule',
      'commitall' => 'timewarp_commitall () { if [ $# -eq 0 ] ; then echo "Error: need a commit message"; exit 1; fi; git submodule foreach "git add -A && git commit -v -a -m \"TIMEWARP autocommit from parent\" ||:" && git add -A && git commit -a -m $@ ; } ; timewarp_commitall',
      'ca' => '!git commitall',
      'pushall' => '!git submodule foreach git push && git push',
      'pa' => '!git pushall',
      'k' => '!gitk --all'
    );
    foreach($gitaliases as $alias => $expansion):
      timewarp_assert(
        drush_shell_cd_and_exec($timewarp->content, "git config --global alias.$alias %s", $expansion)
        , 'Could not add git aliases.');
    endforeach;
    
    // TODO: decide later under which conditions
    // add git_deploy module
    // drush_do_site_command($site_record, 'pm-download', array('git_deploy') )

    return True;
  } catch (Exception $e) {
    return drush_set_error('TIMEWARP_ERROR',
      dt("TIMEWARP: Error: !message\nAborting.", array('!message' => $e->getMessage())));
  }
}

/*
 * process sites
 * 
 * gitnit for every site:
 * * modules (every dir with a .info file in it, but no subdirs of that)
 * * themes (every dir with a .info file in it, but no subdirs of that)
 * * libraries (every subdir of the libraries dir)
 * 
 */
function _timewarp_gitinit_projects_in_all_sites($sites) {
  $projects = array();
  // init repositories for modules/themes (dirs with .info files)
  foreach(new DirectoryIterator($sites) as $item):
    if($item->isDir() and !$item->isDot()):
      $sitedir = $item->getPathname();
      foreach(array('modules', 'themes', 'libraries') as $projects_dir):
        if(is_dir("$sitedir/$projects_dir")):
          _timewarp_gitinit_projects("$sitedir/$projects_dir", $projects, TRUE);
        endif;
      endforeach;
    endif;
  endforeach;
  return $projects;
}

/*
 * find project dirs in the tree and init them,
 * collect paths in &$projects
 */
function _timewarp_gitinit_projects($basedir, &$projects, $is_flat) {
  foreach(new DirectoryIterator($basedir) as $item):
    $dir = $item->getPathName();
    $realdir = realpath($dir);
    if(!$item->isDot() and ($item->isDir() or $item->isLink())):
      if($is_flat || _timewarp_is_project($realdir)):
        $projects[] = $dir;
        _timewarp_gitinit($dir);
      else:
        _timewarp_gitinit_projects($dir, &$projects, $is_flat);
      endif;
    endif;
  endforeach;
}

/*
 * set submodules for projects 
 * move them from sites-content to sites-code
 */
function _timewarp_handle_submodules($timewarp, $submodules) {
  foreach($submodules as $project_old_path):
    if(is_link($project_old_path)) // project already moved
      continue; // no move, no git repo
    $project_relative_path = timewarp_relative_path("$timewarp->sites", $project_old_path);
    $project_new_path = "$timewarp->sitescode/$project_relative_path";
    timewarp_assert(timewarp_moveandlink($project_old_path, $project_new_path), 
      'TIMEWARP: Error creating symlink - already symlinked?');
    // TODO:
    // we're assuming here that all projects have their home at drupal.org
    // this may not be true
    $project_name = basename($project_old_path);
    $project_url = timewarp_projectatdrupalorg($project_name);
    // set origin
    timewarp_assert(drush_shell_cd_and_exec($project_new_path, "git remote add origin $project_url"));
    // add submodule
    timewarp_assert(
      drush_shell_cd_and_exec($timewarp->content, "git submodule add $project_url $timewarp->sitescode/$project_relative_path")
      , "Cannot add submodule $project_name");
  endforeach;
}

/*
 * return git.drupal.org repo for $project
 */
function timewarp_projectatdrupalorg($project_name) {
  return "git://git.drupal.org/project/$project_name.git";
}
