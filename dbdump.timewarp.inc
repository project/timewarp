<?php
// $Id$

require_once 'includes/toolbox.inc';
require_once 'includes/timewarp.inc';

/*
 * timewarp-dbdump command
 * 
 * dumps the db of all sites to timewarp-dbdump dir
 */
function drush_timewarp_dbdump() {
  $timewarp = new TimewarpDirs();
  
  if(!$timewarp->valid):
    return drush_set_error('TIMEWARP_ERROR', 
      dt('TIMEWARP: Cannot dump, we seem not to be in a timewarp site.'));
  endif;
  
  $site_records = _drush_find_local_sites_at_root($timewarp->core);
  
  foreach($site_records as $site_record):
    list($site_root, $site_name) = timewarp_parse_siterecord($site_record);
    $result_file = $timewarp->get_dbdump_path($site_name);
    drush_log("Doing DBDump of site $site_record to $result_file");
    
    $command_options = array(
      'result-file' => $result_file,
      'verbose' => drush_get_option(array('v', 'verbose'), FALSE),
      'debug' => drush_get_option(array('d', 'debug'), FALSE),
      'ordered-dump' => 1,
      'create-db' => 1,
    );
    $backend_options = array(
      'integrate' => TRUE,
    );
    $result = drush_invoke_process($site_record, 'sql-dump', 
      array(), $command_options, $backend_options);
    if(!$result):
      return drush_set_error('TIMEWARP_ERROR', "Error at database dump for site $site_name");
    endif;
  endforeach;
  return TRUE;
}